open Config

module Exn = struct
  exception Finally of exn * exn

  let protect f v ~(finally : 'a -> unit) =
    let res =
      try f v
      with exn ->
        (try finally v with final_exn -> raise (Finally (exn, final_exn)));
        raise exn
    in
    finally v;
    res
end

module Filename = struct
  include Filename

  let split_dir path =        
    let rec split_dir ds p =
      let d = dirname p in
      if d = p then d::ds
      else split_dir (basename p::ds) d 
    in
    split_dir [] path

  let split_extension s = 
    try
      let body = chop_extension s in
      body, 
      String.sub s 
        (String.length body) 
        (String.length s - String.length body)
    with
    | Invalid_argument _ -> s, ""
end

module Hashtbl = struct
  include Hashtbl

  let alter tbl k f =
    let old = find_opt tbl k in
    match old, f old with
    | None, None -> ()
    | _, Some v -> replace tbl k v
    | Some _, None -> remove tbl k
end

module List = struct
  include List

  let rec find_map_opt f = function
    | [] -> None
    | x::xs ->
        match f x with
        | Some v -> Some v
        | None -> find_map_opt f xs

  let unique_by f xs =
    let rec unique st = function
      | [] -> rev st
      | x::xs ->
          let st' = match find_opt (f x) st with
            | Some _ -> st
            | None -> x::st
          in
          unique st' xs
    in
    unique [] xs

  let unique xs = unique_by (=) xs
  let accum xsref x = xsref := x :: !xsref
end

let (+::=) = List.accum

module String = struct
  include String

  let is_prefix ?(from=0) sub str =
    let sublen = String.length sub in
    try 
      String.sub str from sublen = sub
    with _ -> false

  let is_sub' ?from:(pos=0) ~needle:sub str =
    let str_len = String.length str in
    let sub_len = String.length sub in
    if pos + sub_len > str_len then None
    else
      let rec iter pos =
        if pos + sub_len > str_len then None
        else if is_prefix ~from:pos sub str then (Some pos)
        else iter (pos+1)
      in
      iter pos

  let sub' s pos len =
    let orig_len = length s in
    let len = max (min (pos + len) orig_len - pos) 0 in
    sub s pos len
end

module Sys = struct
  include Sys
  let command fmt = Printf.kprintf Sys.command fmt
end

module Unix = struct
  include Unix

  let with_dir path f =
    let dh = opendir path in
    Exn.protect f dh ~finally:(fun _ -> closedir dh)

  (* run [f] on files in [path] *)
  let fold_dir path init f =
    with_dir path @@ fun dh ->
      let rec loop st =
        try
          let st' = f st (readdir dh) in
          loop st'
        with
        | End_of_file -> st
      in
      loop init

  module Inodes = Set.Make(struct
    type t = int * int
    let compare : t -> t -> int = compare
  end)

  module Find = struct

    class path_ ~dir ~base ~depth =
      let path = match Filename.concat dir base with
        | "./." -> "."
        | s -> s
      in
      object (self)
        method dir = dir
        method base = base
        method path = path
        method depth : int = depth
        method stat : (_,[`Exn of exn]) result = try Ok (stat path) with e -> Error (`Exn e)
        method lstat : (_,[`Exn of exn]) result = try Ok (lstat path) with e -> Error (`Exn e)
        method kind : (_,[`Exn of exn]) result = match self#stat with
          | Error e -> Error e
          | Ok stat -> Ok stat.st_kind
        method lkind : (_,[`Exn of exn]) result = match self#lstat with
          | Error e -> Error e
          | Ok stat -> Ok stat.st_kind
        method is_dir = self#kind = Ok S_DIR
        method is_ldir = self#lkind = Ok S_DIR
        method is_reg = self#kind = Ok S_REG
        method dev_inode : (_,[`Exn of exn]) result = match self#stat with
        | Ok stat -> Ok (stat.st_dev, stat.st_ino)
        | Error e -> Error e
      end

    let fold ?(follow_symlink=false) fnames init f =

      let visited = ref Inodes.empty in

      let if_not_visited_then path st f = match path#dev_inode with
        | Error _ -> `Continue, st (* Ignore the error *)
        | Ok inode ->
            if Inodes.mem inode !visited then `Continue, st
            else begin
              visited := Inodes.add inode !visited;
              f st path
            end
      in

      let split_non_dirs_and_dirs pths =
        Fun.flip List.partition pths @@ fun pth ->
          not @@ 
            try 
              if follow_symlink then pth#is_dir else pth#is_ldir 
            with _ -> false
      in

      let get_dir pth =
        fold_dir pth#path [] @@ fun pths -> function
          | "." | ".." -> pths
          | name -> 
              let pth = new path_ ~depth:(pth#depth + 1) ~dir:pth#path ~base:name in
              pth :: pths
      in

      let rec loop pths st =
        let nondirs, dirs = split_non_dirs_and_dirs pths in
        let rec loop g st = function
          | [] -> `Continue, st
          | x::xs ->
              match g st x with
              | `Continue, st -> loop g st xs
              | (`Exit, _ as res) -> res
        in
        match loop find_non_dir st nondirs with
        | `Continue, st -> loop find_dir st dirs
        | (`Exit, _ as res) -> res

      and find_non_dir st pth = match if_not_visited_then pth st f with
        | (`Continue | `Prune), st -> `Continue, st (* /bin/find -prune is only meaningful aginst directories *)
        | (`Exit, _ as res) -> res

      and find_dir st pth = match if_not_visited_then pth st f with
        | (`Exit, _ as res) -> res
        | `Prune, st -> `Continue, st
        | `Continue, st -> loop (get_dir pth) st
      in

      let pths = Fun.flip List.map fnames @@ fun fname -> 
        new path_ ~depth: 0 ~dir:(Filename.dirname fname) ~base:(Filename.basename fname)
      in

      snd @@ loop pths init
  end
end

(* extract and remove the module itself *)
let get_self mname imports =
  match List.partition (fun (s, _d) -> mname = s) imports with
  | [(s,Some d)], others -> (s,d), others
  | [(_,None)], _ -> assert false
  | _ -> assert false

(* cmo format *)
module O = struct
  open Cmo_format (* mli only module *)

  let read_cmo ic =
    let buffer = really_input_string ic (String.length cmo_magic_number) in
    if buffer <> cmo_magic_number then begin
      failwith "Not an object file";
    end;
    let cu_pos = input_binary_int ic in
    seek_in ic cu_pos;
    (input_value ic : compilation_unit)

  let read_cmo_file fn =
    let ic = open_in fn in
    let cmo = read_cmo ic in
    close_in ic;
    cmo

  let cmo_deps cmo =
    let { cu_name (* Name of compilation unit *)
        ; cu_imports (* Names and CRC of intfs imported *)
        ; cu_required_globals (* Compilation units whose initialization
                                 side effects must occur before this
                                 one. *)
        ; _ } = cmo
    in
    let self, imports = get_self cu_name cu_imports in
    let globals =
      (* globals contain predefined exceptions *)
      List.filter_map (fun id ->
          if List.mem id Predef.all_predef_exns then None
          else
            let name = Ident.name id in
            match List.assoc_opt name imports with
            | None -> assert false
            | Some d ->
                Some (name, d)) cu_required_globals
    in
    (self, imports, globals)

end

(* cmi format *)
module I = struct
  include Cmi_format

  let cmi_deps cmi =
    let { cmi_name; cmi_crcs; _ } = cmi in
    get_self cmi_name cmi_crcs
end

module X = struct
  open Cmx_format

  let read_cmx_file fn = Compilenv.read_unit_info fn

  let cmx_deps cmx =
    let { ui_name
        ; ui_imports_cmi
        ; ui_imports_cmx
        ; _ } = cmx
    in
    let self, imports = get_self ui_name ui_imports_cmi in
    let globals =
      (* globals contain predefined exceptions *)
      List.filter_map (fun (name,_) ->
          match List.assoc_opt name imports with
          | None ->
              None
          | Some d ->
              Some (name, d)) ui_imports_cmx
    in
    (self, imports, globals)
end

(* cmi or cmo ? *)
let _guess_format fn =
  let ic = open_in fn in
  let buffer = really_input_string ic (String.length cmo_magic_number) in
  close_in ic;
  if buffer = cmi_magic_number then Some `CMI
  else if buffer = cmo_magic_number then Some `CMO
  else None

module Module : sig
  type t =
    { name : string (*+ ["Foo__bar"] *)
    ; digest : Digest.t
    }

  type module_ = t

  module Set : Set.S with type elt = t
  module Map : Map.S with type key = t

  val find : string list -> Set.t * t list Map.t * string list Map.t
  val pack_name : t -> (string * string) option

  module Graph : sig
    include Graph.Sig.P with type V.t = t
    val add_mdeps : module_ list Map.t -> t -> t
    val reachable_from : t -> (module_ -> bool) -> t
    val reachable_to : t -> (module_ -> bool) -> t
    val transitive_reduction : t -> t
  end

end = struct
  module M = struct
    type t =
      { name : string (*+ ["Foo__bar"] *)
      ; digest : Digest.t
      }

    let equal = (=)
    let compare = compare
    let hash = Hashtbl.hash
  end

  include M

  type module_ = t

  module Set = Set.Make(M)
  module Map = Map.Make(M)

  (** traverse the directories and load cmo and cmi information *)
  let find paths =
    let modules = Hashtbl.create 101 in
    let globals = Hashtbl.create 101 in
    let dirs = Hashtbl.create 101 in
    let aliases = Hashtbl.create 101 in
    let f () path =
      let add_dir m =
        Hashtbl.alter dirs m (function
            | None -> Some [path#dir]
            | Some ds ->
                (* XXX should use a set ? *)
                if not @@ List.mem path#dir ds then
                  Some (path#dir :: ds)
                else Some ds)
      in
      (* .misc is used for dune. XXX put it to the default options *)
      if path#base = ".misc" && path#is_dir then `Prune, ()
      else
        match Filename.split_extension path#base with
        | _, ".cmo" ->
            begin match O.read_cmo_file path#path with
              | exception (Failure e) ->
                  Format.eprintf "Warning: %s: %s@." path#path e;
                  `Continue, ()
              | exception e ->
                  Format.eprintf "Warning: %s: %s@." path#path (Printexc.to_string e);
                  `Continue, ()
              | cmo ->
                  let ((mname, d), _deps, gs) = O.cmo_deps cmo in
                  let m = { name= mname; digest= d } in
                  add_dir m;
                  begin match Hashtbl.find_opt globals m with
                    | None ->
                        let deps, alis = Fun.flip List.partition_map gs @@ function
                            | (mname, Some d) -> Left { name= mname; digest= d }
                            | (mname, None) -> Right mname
                        in
                        Hashtbl.add globals m deps;
                        Hashtbl.add aliases (mname,d) alis
                    | Some _ ->
                        (* should be the same *)
                        ()
                  end;
                  `Continue, ()
            end
        | _, ".cmx" ->
            begin match X.read_cmx_file path#path with
              | exception e ->
                  Format.eprintf "%s: %s@." path#path (Printexc.to_string e);
                  `Continue, ()
              | cmx, _  ->
                  let ((mname, d), _deps, gs) = X.cmx_deps cmx in
                  let m = { name= mname; digest= d } in
                  add_dir m;
                  begin match Hashtbl.find_opt globals m with
                    | None ->
                        let deps, alis = Fun.flip List.partition_map gs @@ function
                            | (mname, Some d) -> Left { name= mname; digest= d }
                            | (mname, None) -> Right mname
                        in
                        Hashtbl.add globals m deps;
                        Hashtbl.add aliases (mname,d) alis
                    | Some _ ->
                        (* should be the same *)
                        ()
                  end;
                  `Continue, ()
            end
        | _, ".cmi" ->
            let ((mname, d), deps) = I.cmi_deps @@ I.read_cmi path#path in
            let m = { name= mname; digest= d } in
            add_dir m;
            begin match Hashtbl.find_opt modules mname with
              | None -> Hashtbl.add modules mname [(d, (path#path, deps))]
              | Some xs ->
                  match List.assoc_opt d xs with
                  | None -> Hashtbl.replace modules mname @@ (d,(path#path, deps)) :: xs
                  | Some _ -> () (* no update, since deps should be the same *)
            end;
            `Continue, ()
        | _ -> `Continue, ()
    in
    Unix.Find.fold ~follow_symlink:true paths () f;
    (* modules, globals, dirs *)
    let ms = Hashtbl.fold (fun m _ st -> Set.add m st) globals Set.empty in
    let deps =
      Hashtbl.fold (fun m ms map ->
          Map.add m ms map) globals Map.empty
    in
    let dirs =
      Hashtbl.fold (fun m ds map ->
          Map.add m ds map) dirs Map.empty
    in
    ms, deps, dirs

  let pack_name { name = mname ; _ } =
    match String.is_sub' ~needle:"__" mname with
    | None -> None
    | Some pos ->
        let postfix =
          (* XXX sub' has a bug *)
          try String.sub' mname (pos+2) 10000 with e -> prerr_endline mname; raise e
        in
        (* There is a module named Cohttp_lwt_unix__ *)
        (* There is a module named Stdlib__list *)
        if postfix <> "" then
          Some (String.sub' mname 0 pos, postfix)
        else None

  module Graph = struct
    module G = Graph.Persistent.Digraph.Concrete(M)
    module O = Graph.Oper.P(G)

    include G

    let add_mdeps mdeps g =
      Map.fold (fun m ms g ->
          let g = add_vertex g m in
          List.fold_left (fun g m' -> add_edge g m m') g ms) mdeps g

    module Reachability = struct
      let rec reachable g visited frontier =
        let succs =
          Set.fold (fun v st ->
              List.fold_left (fun st v -> Set.add v st) st (succ g v))
            frontier Set.empty
        in
        let visited = Set.union visited frontier in
        let frontier = Set.diff succs visited in
        if Set.is_empty frontier then visited
        else reachable g visited frontier

      let reachable is_root g =
        let frontier = fold_vertex (fun v st ->
            if is_root v then Set.add v st else st) g Set.empty
        in
        let set = reachable g Set.empty frontier in
        fun n -> Set.mem n set

      let rec reachable_rev g visited frontier =
        let succs =
          Set.fold (fun v st ->
              List.fold_left (fun st v -> Set.add v st) st (pred g v))
            frontier Set.empty
        in
        let visited = Set.union visited frontier in
        let frontier = Set.diff succs visited in
        if Set.is_empty frontier then visited
        else reachable_rev g visited frontier

      let reachable_rev is_root g =
        let frontier = fold_vertex (fun v st ->
            if is_root v then Set.add v st else st) g Set.empty
        in
        let set = reachable_rev g Set.empty frontier in
        fun n -> Set.mem n set
    end

    let reachable_from g is_root_vertex =
      (* let reachable = Reachability.analyze is_root_vertex g in *)
      let reachable = Reachability.reachable is_root_vertex g in
      fold_vertex (fun n g ->
          if not @@ reachable n then remove_vertex g n else g) g g

    let reachable_to g is_root_vertex =
      (* let reachable = ReachabilityRev.analyze is_root_vertex g in *)
      let reachable = Reachability.reachable_rev is_root_vertex g in
      fold_vertex (fun n g ->
          if not @@ reachable n then remove_vertex g n else g) g g

    let transitive_reduction = O.transitive_reduction ~reflexive:true
  end
end


module Group : sig
  type t =
    | OPAM of string (*+ since it is found in an OPAM build directory *)
    | OCamlc of string (* + since it is found in the OCaml compiler source tree *)
    | Pack of string (*+ Pack__name *)

  type group = t

  val name : t -> string

  val is_opam : string list Module.Map.t -> Module.t -> group option

end = struct

  type t =
    | OPAM of string (*+ since it is found in an OPAM build directory *)
    | OCamlc of string (* + since it is found in the OCaml compiler source tree *)
    | Pack of string (*+ Pack__name *)

  type group = t

  let name = function
    | OPAM s -> "opam:" ^ s
    | OCamlc s  -> "ocaml:" ^ s
    | Pack s -> "pack:" ^ s

  let is_opam dirs m =
    match Module.Map.find m dirs with
    | exception Not_found -> None
    | dirs ->
        Fun.flip List.find_map_opt dirs @@ fun dir ->
          let dircomps = Filename.split_dir dir in (* checked only relatively *)
          let rec f = function
            | ".opam-switch" :: "build" :: dir :: ds when String.is_prefix "ocaml-base-compiler" dir ->
                (* we dig into further to distinguish ocaml-compiler-libs, stdlib, and otherlibs/* *)
                Some (match ds with
                    | ("stdlib" | "boot") :: _ ->
                        (* stdlib modules are found both in stdlib/ and boot/ *)
                        OCamlc "Stdlib"
                    | "otherlibs" :: n :: _ -> OCamlc (String.capitalize_ascii n)
                    | _ -> OCamlc "ocaml-compiler")
            | ".opam-switch" :: "build" :: dir :: _ -> Some (OPAM dir)
            | _::xs -> f xs
            | [] -> None
          in
          f dircomps
end

module Node : sig

  type t =
    | Module of Module.t
    | Grouped of Group.t * t list

  module Set : Set.S with type elt = t

  val name : t -> string

  val match_modules : Re.re -> Set.t -> Module.Set.t
  val filter_set_by_module : (Module.t -> bool) -> Set.t -> Set.t
  val group : (t -> t list) -> Set.t -> Set.t
  val expand : Re.re -> t -> t list
  val opam_pack : string list Module.Map.t -> t -> t list
  module Graph : sig
    include Graph.Sig.P with type V.t = t
    val build : Module.Graph.t -> Set.t -> t
    val write_dot : string -> t -> unit
  end

end = struct
  module M = struct
    type t =
      | Module of Module.t
      | Grouped of Group.t * t list

    let compare = compare
    let hash = Hashtbl.hash
    let equal = (=)
  end

  include M

  module Set = Set.Make(M)

  let modules t =
    let rec f st = function
      | Module m -> Module.Set.add m st
      | Grouped (_, ts) -> List.fold_left f st ts
    in
    f Module.Set.empty t

  let name = function
    | Module m -> m.Module.name
    | Grouped (g, _) -> Group.name g

  let match_modules r ns =
    Set.fold (fun n set ->
        let rec f st = function
          | Module m when Re.execp r m.Module.name -> Module.Set.add m st
          | Module _ -> st
          | Grouped (g, _ns) as n when Re.execp r (Group.name g) -> Module.Set.union (modules n) st
          | Grouped (_, ns) -> List.fold_left (fun st n -> f st n) st ns
        in
        f set n) ns Module.Set.empty

  (* only modules *)
  let filter_by_module p n =
    let rec f n = match n with
      | Module m -> if p m then Some n else None
      | Grouped (g, ns) ->
          match List.fold_left (fun ns n ->
              match f n with
              | None -> ns
              | Some n -> n::ns) [] ns
          with
          | [] -> None
          | ns -> Some (Grouped (g, ns))
    in
    f n

  let filter_set_by_module p ns =
    Set.fold (fun n ns' ->
        match filter_by_module p n with
        | None -> ns'
        | Some n' -> Set.add n' ns') ns Set.empty

  let group (rule : t -> t list) ts =
    let tbl = Hashtbl.create 101 in
    Fun.flip Set.iter ts (fun t ->
        let ts = rule t in
        Fun.flip List.iter ts @@ fun t ->
          let k,ts = match t with
            | Module _ -> None, [t]
            | Grouped (g, ts) -> Some g, ts
          in
          Hashtbl.alter tbl k @@ function
            | None -> Some ts
            | Some ts' -> Some (ts@ts'));
    Hashtbl.fold (fun gopt ts st ->
        match gopt with
        | None -> List.fold_left (Fun.flip Set.add) st ts
        | Some g -> Set.add (Grouped (g, ts)) st) tbl Set.empty

  (* We assume the modules of a Pack never exist in multiple OPAMs.

     opam must be applied before pack.
  *)

  (* XXX We should warn if opam is applied after pack, or implement something good *)
  let opam dirs = function
    | Grouped _ as t -> [t]
    | Module m as t ->
        match Group.is_opam dirs m with
        | None -> [t]
        | Some g -> [Grouped (g, [t])]

  let rec pack = function
    | Grouped (OPAM s, ts) -> [Grouped (OPAM s, List.concat_map pack ts)]
    | Grouped (OCamlc s, ts) -> [Grouped (OCamlc s, List.concat_map pack ts)]
    | Grouped _ as t -> [t]
    | Module m as t ->
        match Module.pack_name m with
        | None -> [t]
        | Some (p,_) -> [Grouped (Pack p, [t])]

  let opam_pack dirs ns = List.concat_map pack @@ opam dirs ns

  let expand r = function
    | Grouped (g, ts) when Re.execp r @@ Group.name g -> ts
    | t -> [t]

  module Graph = struct
    module G = Graph.Persistent.Digraph.Concrete(M)

    include G

    module MG = Module.Graph

    let build mdepg ns =
      let m_to_n =
        Set.fold (fun n map ->
            let ms = modules n in
            Module.Set.fold (fun m map -> Module.Map.add m n map) ms map)
          ns Module.Map.empty
      in
      Set.fold (fun n g ->
          let g = add_vertex g n in
          let ms = modules n in
          let succms =
            Module.Set.fold (fun m succms ->
                MG.succ mdepg m @ succms) ms []
          in
          let succs =
            List.unique @@
            List.fold_left (fun st m ->
                match Module.Map.find_opt m m_to_n with
                | None -> st
                | Some n -> n::st) [] succms
          in
          List.fold_left (fun g n' -> if n <> n' then add_edge g n n' else g) g succs)
        ns empty

    include Graph.Graphviz.Dot(struct

      include G

      let graph_attributes _ = []
      let default_vertex_attributes _ = []

      let quote s = "\"" ^ s ^ "\""

      let vertex_name = function
        | Module m -> quote (m.Module.name ^ "_@_" ^ Digest.to_hex m.Module.digest)
        | Grouped (g, _) -> quote @@ Group.name g

      let vertex_attributes = function
        | Module m -> [`Style `Filled; `Label m.Module.name]
        | Grouped (g, _) -> [`Label (Group.name g)]

      let get_subgraph _ = None
      let default_edge_attributes _ = []
      let edge_attributes _ = []
    end)

    let write_dot fn g =
      let oc = open_out fn in
      output_graph oc g;
      close_out oc
  end
end


module Main = struct

  module MG = Module.Graph
  module G = Node.Graph

  (* build dirs may be updated by new loads *)

  let update_build_dirs bdirs1 bdirs2 =
    let open Module.Map in
    fold (fun m dirs bdirs ->
        update m (function
            | None -> Some dirs
            | Some dirs' -> Some (List.unique (dirs @ dirs')))
          bdirs) bdirs2 bdirs1

  type state =
    { nodes : Node.Set.t
    ; deps : Module.Graph.t
    ; build_dirs : string list Module.Map.t
    }

  let filter_by_module (p : Module.t -> bool) { nodes= ns; deps= mdepg; build_dirs } =
    let ns' = Node.filter_set_by_module p ns in
    let mdepg' =
      MG.fold_vertex (fun m g ->
          if not @@ p m then MG.remove_vertex g m else g) mdepg mdepg
    in
    let build_dirs' = Module.Map.fold (fun m _ build_dirs' ->
        if not (p m) then Module.Map.remove m build_dirs'
        else build_dirs') build_dirs build_dirs in
    { nodes= ns'; deps= mdepg'; build_dirs= build_dirs' }

  let main acts =

    ignore @@ List.fold_left (fun ( { nodes=ns; deps= mdepg; build_dirs } as state) -> function
        | `Transitive_reduction ->
            { state with deps = Module.Graph.transitive_reduction mdepg }
        | `Dump ->
            prerr_endline "dump...";
            Node.Set.iter (fun n -> prerr_endline @@ Node.name n) ns;
            state
        | `Input path ->
            if Sys.is_directory path then begin
              Format.eprintf "Loading modules in directory %s ...@." path;
              let ms, mdeps, build_dirs' = Module.find [path] in
              let ns = Module.Set.fold (fun m ns -> Node.Set.add (Module m) ns) ms ns in
              let mdepg = Module.Graph.add_mdeps mdeps mdepg in
              let build_dirs = update_build_dirs build_dirs build_dirs' in
              { nodes= ns; deps= mdepg;  build_dirs }
            end else assert false
(*
                begin
                match Filename.split_extension path with
                  | _, ".grp" ->
                      Format.eprintf "Loading modules from %s ...@." path;
                      begin match Camlon.Ocaml.load_with G.t_of_ocaml path with
                        | Error (`Conv e) ->
                            (* XXX Camlon.Ocaml should have format_error *)
                            Format.eprintf "Error: %a  Skipped.@."
                              Ocaml_conv.format_error e;
                            g
                        | Error (#Camlon.Ocaml.Parser.error as e) ->
                            Format.eprintf "Error: %a  Skipped.@."
                              Camlon.Ocaml.Parser.format_error e;
                            g
                        | Ok gs' ->
                            List.fold_left (fun g g' ->
                                let g = G.fold_vertex (fun v g -> G.add_vertex g v) g' g in
                                G.fold_edges (fun v1 v2 g -> G.add_edge g v1 v2) g' g)
                              g gs'
                      end
                  | _, ext ->
                      Format.eprintf "%s: unknown extension %s.  Skipping@." path ext;
                      g
              end
*)

        | `OnlyLinkedFrom p ->
            Format.eprintf "Filtering modules only those from %s@." p;
            let ms = Node.match_modules (Re.compile @@ Re.Pcre.re p) ns in
            let mdepg = MG.reachable_from mdepg (fun m -> Module.Set.mem m ms) in
            let ms = MG.fold_vertex Module.Set.add mdepg Module.Set.empty in
            filter_by_module (fun m -> Module.Set.mem m ms) state

        | `OnlyDependentOn p ->
            Format.eprintf "Filtering modules only those dependent on %s@." p;
            let ms = Node.match_modules (Re.compile @@ Re.Pcre.re p) ns in
            let mdepg = MG.reachable_to mdepg (fun m -> Module.Set.mem m ms) in
            let ms = MG.fold_vertex Module.Set.add mdepg Module.Set.empty in
            filter_by_module (fun m -> Module.Set.mem m ms) state

        | `Exclude p ->
            Format.eprintf "Excluding modules match with %s@." p;
            let ms = Node.match_modules (Re.compile @@ Re.Pcre.re p) ns in
            filter_by_module (fun m -> not @@ Module.Set.mem m ms) state

        | `Match p ->
            Format.eprintf "Filter modules to only those match with %s@." p;
            let ms = Node.match_modules (Re.compile @@ Re.Pcre.re p) ns in
            filter_by_module (fun m -> Module.Set.mem m ms) state

        | `Expand p ->
            Format.eprintf "Expanding module groups match with %s@." p;
            let pat = Re.compile @@ Re.Pcre.re p in
            { state with nodes= Node.(group (expand pat) ns) }

(*
        | `Pack p ->
            Format.eprintf "Packing modules matching with %s@." p;
            let pat = Re.compile @@ Re.Pcre.re p in
            let vs =
              G.fold_vertex (fun v vs ->
                if Re.execp pat @@ name_of_node v then v::vs
                else vs) g []
            in
            let g = unify_vertex g vs (Pack p) in
            g_with_report g
*)

        | `OPAMPackAuto ->
            Format.eprintf "Packing modules automatically...@.";
            { state with nodes= Node.(group (opam_pack build_dirs) ns) }

        | `Output path ->
            begin match Filename.split_extension path with
            | _, ".dot" ->
                Format.eprintf "Writing to %s@." path;
                let g = G.build mdepg ns in
                G.write_dot path g;
                state
            | _, ".pdf" ->
                Format.eprintf "Rendering the graph to %s ...@." path;
                let g = G.build mdepg ns in
                let dot = Filename.temp_file "cmtraverse" "dot" in
                G.write_dot dot g;
                ignore @@ Sys.command "dot -Tpdf -o%s %s" (Filename.quote path) (Filename.quote dot);
                state

(*
            | _, ".grp" ->
                Format.eprintf "Writing to %s@." path;
                Camlon.Ocaml.save_with G.ocaml_of_t ~perm:0o644 path [g];
                g

*)
            | _, ext ->
                Format.eprintf "%s: unknown extension %s.  Skipping@." path ext;
                state
            end
      ) { nodes= Node.Set.empty; deps= MG.empty; build_dirs= Module.Map.empty } acts


  let opts =
    let rev_opts = ref [] in
    Arg.(parse
           [ "-f", String (fun s -> rev_opts +::= `OnlyLinkedFrom s),
             "<rex> : filter modules to only linked from the modules match with the pattern";

             "-t", String (fun s -> rev_opts +::= `OnlyDependentOn s),
             "<rex> : filter modules to only linked to the modules with match with the pattern";

             "-e", String (fun s -> rev_opts +::= `Exclude s),
             "<rex> : exclude modules which match with the pattern";

             "-m", String (fun s -> rev_opts +::= `Match s),
             "<rex> : filter modules to only those which match with the pattern";

             "-x", String (fun s -> rev_opts +::= `Expand s),
             "<rex> : expand groups match with the pattern";

(*
             "-p", String (fun s -> rev_opts, +::= `Pack s),
             "<path> : unify modules match with the pattern";
*)

             "-A", Unit (fun () -> rev_opts +::= `OPAMPackAuto),
             " : automatically group modules";

             "-o", String (fun s -> rev_opts +::= `Output s),
             "<path> : output the current graph to the path";

             "-d", Unit (fun () -> rev_opts +::= `Dump),
             " : show the current modules";

             "-r", Unit (fun () -> rev_opts +::= `Transitive_reduction),
             " : simply the graph by transtive reduction";
           ]
           (fun s -> rev_opts +::= `Input s)
           "cmgraph [dirs files actions ..]");
    List.rev !rev_opts

  let () = main opts
end
