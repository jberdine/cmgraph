# cmgraph: OCaml module dependency graph generator

cmgraph is a tool to scan OCaml compiled module files and build their dependency graph.

## Overview

cmgraph scans compiled modules `.cm*`, not source code.  Therefore it need not any complicated compiler switch tweaks like include dirs, PPX settings, etc.  Just scrape the compiled modules you are interested, then  you can get the same dependency graph as the compiler sees.

Module dependency graphs of middle and larger projects are too complex to visualize.  cmgraph has command line options to group, expand and filter modules to hide the details you are not interested in.

## Command line arguments

cmgraph has an internal state of a dependency graph, which starts from the empty.
It processes the given arguments from left to right, which accumulate, filter,
or save the graph state.

### Directory

Directory arguments without switches scan the compiled modules (`.cmo` and `.cmx`)
in the directories recursively and add their dependency information to the current graph.

### `-A` : Automatically group modules

Group modules by OPAM build dirs like `opam:stdlib` 
and module names (like `Pack__module` and `Pack__module2` to `pack:Pack`).

This option is almost necessary when you create a graph of many modules,
since dependency graph rendering by Graphviz takes very long time for many nodes.
The best practice is to use `-A` to group modules first, then use the following
`-x` to expand some of them which you are interested in.

### `-x <rex>`

EXpand the toplevel grouped modules match with the pattern.

The names displayed on the graph are matched with `<rex>`, a PCRE. It applies to the other options with `<rex>`

### `-f <rex>` : Only linked from

Filter the current graph and restrict its modules to those only linked directly 
or indirectly from the modules match with the regular expression,
including the matched modules themselves.

### `-t <rex>` : Only reachable to

Filter the current graph and restrict its modules to those only dependent 
directly or indirectly on the modules match with the regular expression,
including the matched modules themselves.


### `-o <file>` : Output the current graph

`-o` switch saves the current dependency graph to a file. 
The format of the graph is determined by the file extension:

* `.dot`: saves the graph in Graphviz's dot format.
* `.pdf`: saves the graph in PDF, using Graphviz's `dot` command. (The command must be accessible in the `PATH`.)

## Example

You *can* scrape the modules of OCaml compiler and make a dependency graph PDF as follows:

```
$ cmgraph ~/.opam/4.11.1/.opam-siwtch/build/ocaml-base-compiler.4.11.1 -o 1.pdf
```

The graph is very huge and it takes long time to make the PDF for graphviz.  The output PDF is hard to understand for human beings.  Because of its huge size, its image is completely blurred:

![](img/1-vanilla.pdf.png)

You can simplify the graph complexity with `-r` option to reduce transitivity:

```
$ cmgraph ~/.opam/4.11.1/.opam-siwtch/build/ocaml-base-compiler.4.11.1 -r -o 1.pdf
```

![](img/tred.png)

For huge graphs you should use `-A` option before the rendering option `-o 1.pdf` in order to automatically group modules:

```
$ cmgraph ~/.opam/4.11.1/.opam-siwtch/build/ocaml-base-compiler.4.11.1 -A -o 2.pdf
```

![](img/2-A.pdf.png)

Now it is over-summarized.  Let's expand `ocaml:Stdlib` with `-x Stdlib`:

```
$ cmgraph ~/.opam/4.11.1/.opam-siwtch/build/ocaml-base-compiler.4.11.1 -A -x Stdlib -o 3.pdf
```

![](img/3-xStdlib.pdf.png)

It shows some internal structure of "Stdlib" but the structure of the OCaml standard library is not visible.  Let's expand `pack:Stdlib` with another `-x Stdlib`:

```
$ cmgraph ~/.opam/4.11.1/.opam-siwtch/build/ocaml-base-compiler.4.11.1 -A -x Stdlib -x Stdlib -o 4.pdf
```

![](img/4-xStdlib.pdf.png)

Ok... now it is enough detailed but the graph is big.

You may be only interested in which modules `Stdlib__list` depends on.
In that case you can use `-f list` to filter out the unrelated modules:

```
$ cmgraph ~/.opam/4.11.1/.opam-siwtch/build/ocaml-base-compiler.4.11.1 \
  -A -x Stdlib -x Stdlib -f list \
  -o 5.pdf
```

![](img/5-flist.pdf.png)

Or... you may be only interested in the opposite, which modules depend on `Stdlib__list`.  Use `-t list` to filter out the others:

```
$ cmgraph ~/.opam/4.11.1/.opam-siwtch/build/ocaml-base-compiler.4.11.1 \
  -A -x Stdlib -x Stdlib -t list \
  -o 6.pdf
```

![](img/5-tlist.pdf.png)


You may use `-m Stdlib` to show only the modules of `Stdlib`:

```
$ cmgraph ~/.opam/4.11.1/.opam-siwtch/build/ocaml-base-compiler.4.11.1 \
  -A -x Stdlib -x Stdlib -m Stdlib \
  -o 7.pdf
```

![](img/6-mStdlib.pdf.png)

You can simplify the graph further with transitive reduction of `-r`:

```
$ cmgraph ~/.opam/4.11.1/.opam-siwtch/build/ocaml-base-compiler.4.11.1 \
  -A -x Stdlib -x Stdlib -m Stdlib -r \
  -o 8.pdf
```

![](img/stdlib-tred.png)
